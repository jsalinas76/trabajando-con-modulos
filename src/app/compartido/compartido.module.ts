import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Modulo1Module } from '../modulo1/modulo1.module';
import { Modulo2Module } from '../modulo2/modulo2.module';
import { Modulo3Module } from '../modulo3/modulo3.module';
import { Modulo4Module } from '../modulo4/modulo4.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    Modulo1Module,
    Modulo2Module,
    Modulo3Module,
    Modulo4Module
  ],
  exports: [
    Modulo1Module,
    Modulo2Module,
    Modulo3Module,
    Modulo4Module
  ]
})
export class CompartidoModule { }
