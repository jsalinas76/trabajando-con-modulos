import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisorPresupuestoComponent } from './visor-presupuesto.component';

describe('VisorPresupuestoComponent', () => {
  let component: VisorPresupuestoComponent;
  let fixture: ComponentFixture<VisorPresupuestoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisorPresupuestoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisorPresupuestoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
