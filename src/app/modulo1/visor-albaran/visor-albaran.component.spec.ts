import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisorAlbaranComponent } from './visor-albaran.component';

describe('VisorAlbaranComponent', () => {
  let component: VisorAlbaranComponent;
  let fixture: ComponentFixture<VisorAlbaranComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisorAlbaranComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisorAlbaranComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
