import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FacturaComponent } from './factura/factura.component';
import { VisorAlbaranComponent } from './visor-albaran/visor-albaran.component';
import { VisorPresupuestoComponent } from './visor-presupuesto/visor-presupuesto.component';
import { LineaComponent } from './linea.component';

@NgModule({
  declarations: [
    FacturaComponent,
    VisorAlbaranComponent,
    VisorPresupuestoComponent,
    LineaComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [FacturaComponent, VisorPresupuestoComponent]
})
export class Modulo1Module { }
