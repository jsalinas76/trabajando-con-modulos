import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModEnrutadoRoutingModule } from './mod-enrutado-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ModEnrutadoRoutingModule
  ]
})
export class ModEnrutadoModule { }
