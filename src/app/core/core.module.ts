import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompartidoModule } from '../compartido/compartido.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CompartidoModule
  ]
})
export class CoreModule { }
